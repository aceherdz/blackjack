import random

def inicializarBaraja(numMazos):
    baraja = []
    pintas = [ "\u2663", "\u2660", "\u2661", "\u2662" ]
    for i in range(2):
        for pinta in pintas:
            for valor in range(1,14):
                baraja.append({"pinta":pinta, "valor": valor})
    random.shuffle(baraja)    
    return baraja

def definirJugadores():
    listo = False
    jugadores = []
    while not listo:
        nombre=input('Ingrese Nombre Jugador :')        
        if nombre == "":
            listo = True
        else:
            jugadores.append({ "nombre": nombre, "cartas": []})
    print ('jugadores:'  )
    for i in jugadores:
        print ('   ' + i["nombre"])
    return jugadores

def repartirCartas(jugador,cartas):
    darCarta(jugador,cartas)
    darCarta(jugador,cartas)

def darCarta(jugador,cartas):
    jugador["cartas"].append(cartas.pop(0))

def revisarJuego(jugador):
    print ('')
    print ("--------" , jugador ["nombre"] , "-------------------------")
    print (' su juego es ' , jugador["cartas"])
    conteo=contarCartas(jugador["cartas"])
    print ('             ' , conteo)
    if len(conteo) > 0:
        salida = input ('desea pedir otra carta [y]')
        if salida == "n" or salida == "N":
            return False
        else:
            return True
    else:
        print ('estas fuera!')
        return False




def contarCartas(cartas):
    totales =[] 
    for carta in list(cartas):
        valor = carta["valor"]
        if valor > 10:
           valor = 10
        if valor == 1:
            if len(totales) == 0:
                totales.append(1)
                totales.append(11)
            else:
                totalesTemp =[]
                for total in totales:
                    totalesTemp.append(total+1)
                    totalesTemp.append(total+11)
                totales = totalesTemp
        else:
            if len(totales) == 0:
                totales.append(valor)
            else:
                totales[:] = [x + valor for x in totales]
    #retirar los duplicados
    totales = list( dict.fromkeys(totales))
    #retirar los mayores de 21
    totales = [s for s in totales if s <= 21]
    return totales



def revisarGanador(jugadores):
    print ("  JUEGO")
    for jugador in jugadores:
        print (jugador)
    ganadores = []
    blackjacked = False
    for jugador in jugadores:
        resultado = contarCartas(jugador["cartas"])
        if len(resultado) > 0:
            if isBlackJack(jugador["cartas"]):
                ganadores.append({"jugador":jugador,"puntaje":"bj"})
                retirarNoBJGanadores(ganadores)
                blackjacked = True
            elif not blackjacked:
                topPuntaje = mejorPuntaje(resultado)
                if len(ganadores) == 0:
                    ganadores.append({"jugador":jugador,"puntaje":topPuntaje})
                else:
                    agregarNuevoGanador(topPuntaje,jugador,ganadores)

    print ("!!!!! GANADOR !!!!!")
    for ganador in ganadores:
        print (ganador)


def agregarNuevoGanador(topPuntaje,jugador,ganadores):
    oldGanadores = ganadores.copy()
    for ganador in oldGanadores:
        if topPuntaje > ganador["puntaje"]:
            ganadores.remove(ganador)
            ganadores.append({"jugador":jugador,"puntaje":topPuntaje})
        elif topPuntaje == ganador["puntaje"]:
            ganadores.append({"jugador":jugador,"puntaje":topPuntaje})

def retirarNoBJGanadores(ganadores):
    oldGanadores = ganadores.copy()
    for ganador in oldGanadores:
        if ganador["puntaje"] != "bj":
            ganadores.remove(ganador)

def mejorPuntaje(resultado):
    return max(resultado)


def isBlackJack(cartas):    
    if len(cartas) != 2:
        return False
    if cartas[0]["valor"] == 1 and  cartas[1]["valor"] > 10:
        return True
    if cartas[0]["valor"] > 10 and  cartas[1]["valor"] == 1:
        return True        
    return False


def game():
    jugadores={}
    cartas=inicializarBaraja(2)
    jugadores = definirJugadores()
    for i in jugadores:
        repartirCartas(i,cartas)
    for jugador in jugadores:
        while revisarJuego(jugador):
           darCarta(jugador,cartas)
    revisarGanador(jugadores)    


#revisarGanador(
#    [{'nombre': 'Tito', 'cartas': [{'pinta': '♣', 'valor': 13}, {'pinta': '♠', 'valor': 10}]},
#     {'nombre': 'German', 'cartas': [{'pinta': '♡', 'valor': 6}, {'pinta': '♠', 'valor': 9}, {'pinta': '♣', 'valor': 1}, {'pinta': '♣', 'valor': 11}]},
#     {'nombre': 'Jose', 'cartas': [{'pinta': '♡', 'valor': 13}, {'pinta': '♣', 'valor': 5}, {'pinta': '♠', 'valor': 2}, {'pinta': '♠', 'valor': 4}]}, # <- el deberia ganar
#     {'nombre': 'Javier', 'cartas': [{'pinta': '♢', 'valor': 8}, {'pinta': '♡', 'valor': 1}]},
#     {'nombre': 'Luis', 'cartas': [{'pinta': '♠', 'valor': 4}, {'pinta': '♣', 'valor': 6}, {'pinta': '♢', 'valor': 9}, {'pinta': '♣', 'valor': 13}]},
#     {'nombre': 'Oscar', 'cartas': [{'pinta': '♣', 'valor': 9}, {'pinta': '♢', 'valor': 1}]}]
#)

#print (isBlackJack([{"valor":11},{"valor":1}]))
#print (isBlackJack([{"valor":2},{"valor":1}]))
#print (isBlackJack([{"valor":5},{"valor":8}]))
#print (isBlackJack([{"valor":10},{"valor":1}]))
#print (isBlackJack([{"valor":13},{"valor":1}]))

#contarCartas({"valor":10},{"valor":10}) # 11, 21 
#contarCartas({"valor":13},{"valor":1}) # 11, 21
#contarCartas({"valor":1},{"valor":2},{"valor":1},{"valor":1}) # 11, 21
#contarCartas({"valor":10},{"valor":13},{"valor":11}) # vacio

game()
           


